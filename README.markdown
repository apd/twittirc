**OAuth support is in.**
**To use it you'll need to create your own**
**consumer key with Twitter.*** 


**You'll need to install a few ruby gems:**

	gem install twitter
	gem install term-ansicolor
	gem install parseconfig
	gem install cgi
	gem install httparty
	gem install shellwords

**Note:** *You may need to install libopenssl-ruby
if you haven't already.*  


####Quick and Dirty####

**Create a ~/.twitterrc file:** 
*Here's the minimum config to get things rolling*

	user = blah
	interval = 30


####IMAP####

**To add imap checking:**

	mailboxes=gmail,somedomain

	gmail_server=imap.gmail.com
	gmail_login=woot@gmail.com
	gmail_password=mypassword

	somedomain_server=imap.somedomain.com
	somedomain_login=asdf@somedomain.com
	somedomain_password=mypassword  


####Keychain####

*If you have the credentials for your twitter or mail account 
stored in an OS X keychain, you can set the password field for
that service to 'OSX-Keychain' to extract the saved keychain 
password.  It should find the username.  Keychain support plays
pretty fast and loose.*  


####Notifications####

**If you would like to play a sound when someone sends you a 
message**

	sound = /usr/share/sounds/some.wav  


**Enabling Growl notifications in OS X:**

Install growlnotify.  
*Instructions: http://growl.info/documentation/growlnotify.php*

Enable support in ~/.twitterrc

	notifier=growl
	twittericon = /path/to/image (optional)
	mailicon = /path/to/image (optional - separate icon for mail)  
	

**Enabling libnotify notifications in Gnome**

Install notify-send (used to send messages to libnotify)
On Ubuntu: 

	apt-get install libnotify-bin
  
*Other distributions may require more/other steps*  


Enable support in ~/.twitterrc:

	notifier=libnotify
	twittericon = /path/to/image (optional)
	mailicon = /path/to/image (optional - separate icon for mail)


**Note:** *Icons must be in PNG, JPEG, TIFF, PICT, PDF, JPEG 2000, 
Adobe Photoshop, BMP, .ico, or .icns format.*  


####Links####

Enable support for automatically following links:

	browser = "/path/to/browser args"
	
**Note:** *Browser should support having its url passed as the
last command line argument.  Google Chrome and Mozilla Firefox 
have been lightly tested.  


Enable support for uploading to Instapaper:

	instapaper_user = "Your instapaper username"
	instapaper_pass = "Your instapaper password" # If set







