#!/usr/bin/ruby

require 'rubygems'
require 'twitter'
require 'term/ansicolor'
require 'parseconfig'
require 'net/imap'
require 'net/smtp'
require 'shellwords'
require 'cgi'
require 'httparty'

class String
   include Term::ANSIColor
end

def follow(user, client)
	begin
		client.friendship_create(client.user(user).id, true)
		puts "You are now following #{user}".yellow
	rescue Exception => e
		puts "Something went wrong while attempting to follow #{user}: #{e.to_s}".red
	end
end

def unfollow(user, client)
	begin
		client.friendship_destroy(client.user(user).id)
		puts "You are no longer following #{user}".yellow
	rescue Exception => e
		puts "Something went wrong while attempting to unfollow #{user}: #{e.to_s}".red
	end
end

def usage
	puts "/help - What you are currently viewing"
	puts "/timeline - Display your friends timelime"
	puts "/checkmail - Check your mailboxes for new messages"
	puts "/rlist - List tweets that you were most recently included in"
	puts "/dlist - List your most recent direct messages"
	puts "/update <message> - Update your status"
	puts "/search <query> - Search for the specified query"
	puts "/retweet <user> - Retweet the last update from the specified user"
	puts "/reply <message> - Send a message to the last person that included you in a message"
	puts "/dreply <message> - Send a message to the last person that direct messaged you"
	puts "/dmesg <user> <message> - Send a direct message to a user"
	puts "/who <user> - Get information about a specific user"
	puts "/follow <user> - Follow a specific user"
	puts "/unfollow <user> - Unfollow a specific user"
	puts "/url <0-n> - Load URL by its associated numeric ID"
	puts "/urlsave or /us - Send to Instapaper" 
	puts "/quit - What do you think it does?"
end

def extract_keychain_password(user)
  password = `security 2>&1 > /dev/null find-generic-password -ga #{user}`
  password = password.gsub(/password: "([^"]+?)"/, '\1').chomp.to_s
  return password
end

def oauth_authorize
	begin
		Twitter.configure do |config|
			config.consumer_key = $config.get_value('oauth_consumer_key')
			config.consumer_secret = $config.get_value('oauth_consumer_secret')
			config.oauth_token = $config.get_value('oauth_atoken')
			config.oauth_token_secret = $config.get_value('oauth_asecret')
		end

		return Twitter::Client.new

	rescue Exception => e
		puts "Oauth Failed #{e.to_s}".yellow
	end
end

def display_user_info(u)
	begin
		puts "#{u.name} (".yellow + u.screen_name + ")".yellow
		puts "Profile: ".yellow + "http://www.twitter.com/#{u.screen_name}"
		puts "Last Message: ".yellow + "#{u.status.text}"
	rescue Exception => e
		puts "User not found: #{e.to_s}".yellow
	end
end

def format_time(created_at)
	Time.parse(created_at).strftime("%D %T").to_s
end


def popup_notify(desc, body, mail=false)
  begin
    #Escape quotes for shell args
    body = body.uncolored
    body = body.unpack("U*").map{|c|c.chr}.join.shellescape
    if $notifier== 'libnotify' && $firstrun.nil?
      if mail
        if $mailicon
          Kernel.system("notify-send --icon=#{$mailicon} #{desc} #{body}")
				else
	  			Kernel.system("notify-send #{desc} #{body}")
				end
      else
        if $twittericon
          Kernel.system("notify-send --icon=#{$twittericon} #{desc} #{body}")
				else
	  			Kernel.system("notify-send #{desc} #{body}")
				end
      end
    elsif $notifier == 'growl' && $firstrun.nil?
      if mail
        if $mailicon
          Kernel.system("growlnotify #{desc} --image #{$mailicon} -m #{body}")
        else
          Kernel.system("growlnotify #{desc} -m \"#{body}\"")
        end
      else
        if $twittericon
          Kernel.system("growlnotify #{desc} --image #{$twittericon} -m #{body}")
        else
          Kernel.system("growlnotify #{desc} -m #{body}")
        end
      end
    end
  rescue Exception => e
      puts "Notification daemon error: #{e.to_s}".yellow
  end
end

def urlview(args)
  begin

	  if args =~ /[0-9]+/
 	    Kernel.system("#{$browser} #{@urls[args.to_i].unpack("U*").map{|c|c.chr}.join.shellescape} > /dev/null 2>&1 &")	
	  end
  rescue Exception => e
	puts "Error in urlview(): #{e.to_s}".yellow
  end
end

def urlsave(args)
	begin
		res = HTTParty.get('https://www.instapaper.com/api/add', 
			:query => { :username => "#{$instapaper_user}",
			:password => "#{$instapaper_pass}",
			:url => "#{@urls[args.to_i]}" })

		if res.code.to_s == '201'
			puts "Sent to Instapaper"
		else
			puts "Send to Instapaper failed: #{res.code.to_s} #{res.message.to_s}".red
		end
		
	rescue Exception => e
		puts "Send to Instapaper failed: #{e.to_s}".yellow
	end
end

def display_message(message, sound=false)
	if  message.text =~ /((http:\/\/.+?)(\s|$))/
		@urls << $1 
	end
	
	urlid = @urls.length - 1
	
	message.text = CGI.unescapeHTML(message.text)
	message.text = message.text.gsub(/(@[a-zA-Z0-9_]+)/, '\1'.green)
	message.text = message.text.gsub(/(http:\/\/.+?)(\s|$)/, '['.magenta + urlid.to_s + '] \1 '.magenta)

	if message.recipient 
          puts "#{format_time(message.created_at)}".blue + " #{message.sender.screen_name}: ".yellow + "#{message.text}".green
  
          if $notifier
            popup_notify(message.sender.screen_name, message.text)
          end 
         

	elsif message.text =~ /#{$user}/
		return if $ignored && $ignored.include?(message.user.screen_name)  

		if sound
			Kernel.system("play -q #{$sound}")
		end

		puts "#{format_time(message.created_at)}".blue + " #{message.user.screen_name}: ".yellow + "#{message.text}".red
  
    		if $notifier
		      popup_notify(message.user.screen_name, message.text)
   		 end
          
	else
		return if $ignored && $ignored.include?(message.user.screen_name)  

		if sound
			Kernel.system("play -q #{$sound}")
		end

		if message.in_reply_to_status_id
			reply = $client.status(message.in_reply_to_status_id)

			reply.text = reply.text.gsub(/(@[a-zA-Z0-9]+)/, '\1'.green) 
		    
			puts "#{format_time(message.created_at)}".blue + " #{message.user.screen_name}: ".yellow + "#{message.text} -- " + "in reply to ".blue + "@#{reply.user.screen_name} ".yellow + "  #{reply.text}"

      			if $notifier
        	    		popup_notify(message.user.screen_name, message.text)
      			end
            
		else
			puts "#{format_time(message.created_at)}".blue + " #{message.user.screen_name}: ".yellow + "#{message.text}"

			if $notifier
				popup_notify(message.user.screen_name, message.text)
			end
		end
	end
end

def search(query, client)
	begin
		search = Twitter::Search.new

		if query =~ /#/
			search.hashed(query)	
		elsif query =~ /@/
			search.from(query)
		else
			search.containing(query)
		end
		
		search.each do |message|
			begin
				puts "#{format_time(message.created_at)}".blue + " #{client.user(message.from_user_id).screen_name}: ".yellow + "#{message.text}"
			rescue Exception => e
				# We don't want to do anything here
			end
		end

	rescue Exception => e
		puts e.to_s.red
	end
end

def update_messages(messages, store=true, sound=false, show=true)
	messages = messages.sort {|a,b| Time.parse(a.created_at) <=> Time.parse(b.created_at)}

  	messages.each do |message|
		if store
			if $seen_messages[message.id].nil?
				$seen_messages[message.id] = true
				
				if show
					display_message(message, sound)
				end
			end	
		else
			display_message(message, sound)
		end
	end
end

def send_direct_message(options, client)
	if options =~ /^(.*?) (.*)/
		client.direct_message_create($1, $2)
	else
		usage
	end
end

def connect_email_mailbox(server, login, password)
	imap = Net::IMAP.new(server, 993, true)
	imap.login(login, password)
	imap.select('INBOX')

	return imap
end

def update_email_messages
	@mailboxes.each do |mailbox|
		if mailbox['connection'].nil? || mailbox['connection'].disconnected?
			mailbox['connection'] = connect_email_mailbox(
					mailbox['server'], mailbox['login'], mailbox['password']
			)
		end
		
		mailbox['connection'].select('INBOX')	
		
		begin
			mailbox['connection'].search(["NOT", "DELETED"]).each do |message|
				uid = mailbox['connection'].fetch(message, "UID")[0].attr["UID"]

				if mailbox['messages'][uid].nil?	
					env = mailbox['connection'].fetch(message, "ENVELOPE")[0].attr["ENVELOPE"]
					mailbox['messages'][uid] = "#{env.from[0].name}: #{env.subject}"
					puts "#{format_time(Time.now.to_s)}".blue + " New message for #{mailbox['login']}: ".yellow + mailbox['messages'][uid].red
					popup_notify(mailbox['login'], mailbox['messages'][uid], true)
				end
			end

		rescue Exception => e
			puts "Error while checking mailbox for #{mailbox['login']}: #{e.to_s}".red
		end
	end
end

def add_email_mailbox(server, login, password)
	begin
		@mailboxes << { 
				'server' => server,
				'login' => login,
				'password' => password,
				'messages' => {},
				'connection' => connect_email_mailbox(server, login, password)
		}
	
	rescue Exception => e
		puts "Error while connecting to mailbox #{login}: #{e.to_s}"
	end
end
		
begin
	@mailboxes = []
	@urls = []
	if File.exists?(File.expand_path('~/.twitterrc'))
		$config = ParseConfig.new(File.expand_path('~/.twitterrc'))
		$user = $config.get_value('user')
		$password = $config.get_value('password')
		if $password == "OSX-Keychain"
		  $password = extract_keychain_password($user)
		end
		$sound = $config.get_value('sound')
		$notifier = $config.get_value('notifier')
		$interval = $config.get_value('interval') ? $config.get_value('interval').to_i : 30
		$twittericon = $config.get_value('twittericon') 
		$mailicon = $config.get_value('mailicon')
		$browser = $config.get_value('browser')

		if $config.get_value('ignored')
			$ignored = []

			$config.get_value('ignored').split(",").each do |ig|
				$ignored << ig 
			end
		end
		
			
		if $config.get_value('instapaper_user')
			$instapaper_user = $config.get_value('instapaper_user')
			$instapaper_pass = $config.get_value('instapaper_pass') if $config.get_value('instapaper_pass')
		end 
				
		if $config.get_value('mailboxes')	
			$config.get_value('mailboxes').split(",").each do |mailbox|
				server = $config.get_value("#{mailbox}_server")
				login = $config.get_value("#{mailbox}_login")
				password = $config.get_value("#{mailbox}_password")
				
				if password == "OSX-Keychain"
				  password = extract_keychain_password(login)
				end
				
				add_email_mailbox(server, login, password)
				
			end
		end
		
		#We don't want our notifier to spam us at startup
		$firstrun = true	
 
	else
		throw Exception.new("You must create a ~/.twitterrc file.")
	end

	#if $user.nil? || $password.nil?
	#	throw Exception.new("You must specify at least your user and password in your ~/.twitterrc file")
	#end

rescue Exception => e
	puts e.to_s.red
	exit
end

$seen_messages = {}
$client = oauth_authorize

# Only show your friends timelime on start up
begin
	update_email_messages
	update_messages($client.friends_timeline)
	update_messages($client.direct_messages, true, false, false)
	$firstrun = nil
rescue Exception => e
	puts e.to_s.red
end

ctr = 1
while true
	if (ctr % $interval) == 0
		ctr = 0
		begin	
			update_email_messages
			update_messages($client.friends_timeline + $client.direct_messages)
		rescue Exception => e
			puts "Error while updating: #{e.to_s}".red
		end
	end	

	if IO.select([$stdin], nil, nil, 0.1)
		line = gets

		if line =~ /^\/(.*?)\s+(.*?)$/
			command = $1 
			options = $2

			if command == 'update'
  				$client.update(options)
			elsif command == 'retweet'
				$client.update("RT @#{options}: #{$client.user(options).status.text}")
			elsif command == 'help'
				usage
			elsif command == 'reply'
				$client.update("@#{$client.mentions[0].user.screen_name} #{options}")
			elsif command == 'dreply'
				send_direct_message("#{$client.direct_messages[0].sender.screen_name} #{options}", $client)
			elsif command == 'dmesg'
				send_direct_message(options, $client)
			elsif command == 'rlist'
				update_messages($client.mentions, false, false)
			elsif command == 'dlist'
				update_messages($client.direct_messages, false, false)
			elsif command == 'timeline'
				update_messages($client.friends_timeline, false, false, true)
			elsif command == 'who'
				display_user_info($client.user(options))
			elsif command == 'follow'
				follow(options, $client)
			elsif command == 'unfollow'
				unfollow(options, $client)
			elsif command == 'search'
				search(options, $client)
			elsif command == 'checkmail'
				update_email_messages
			elsif command == 'url'
				urlview(options)	
			elsif command == 'urlsave' || command == 'us'
				urlsave(options)
			elsif command == 'quit'
				exit
			end
		else
			usage
		end
	end
	
	sleep 1
	ctr = ctr + 1
end
